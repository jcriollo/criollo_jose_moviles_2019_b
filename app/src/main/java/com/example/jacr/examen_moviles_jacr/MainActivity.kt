package com.example.jacr.examen_moviles_jacr

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import com.raywenderlich.timefighter.R

class MainActivity : AppCompatActivity() {
    internal lateinit var tapMeButton: Button
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView

    internal var score = 0
    internal var gameStarted=false
    internal lateinit var countDownTimer:CountDownTimer
    internal val initialCountDown: Long=60000
    internal val countDownInterval: Long=1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tapMeButton = findViewById(R.id.tapMeButton)
        gameScoreTextView = findViewById(R.id.gameScoreTextView)
        timeLeftTextView = findViewById(R.id.timeLeftTextView)

        tapMeButton.setOnClickListener { view ->
            incrementScore()
        }

        gameScoreTextView.text = getString(R.string.yourScore, score)
    }

    private fun resetGame(){
        score=0
        gameScoreTextView.text=getString(R.string.yourScore,score)
        var initialTimeLeft=initialCountDown/1000
        timeLeftTextView.text=getString(R.string.timeLeft, initialTimeLeft)
        countDownTimer=object : CountDownTimer(initialCountDown,countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                val timeLeft=millisUntilFinished/1000
                timeLeftTextView.text=getString(R.string.timeLeft, timeLeft)
                }

            override fun onFinish() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
        gameStarted=false
    }

    private fun incrementScore() {
        score += 1
        val newScore = getString(R.string.yourScore, score)
        gameScoreTextView.text = newScore
    }
}

